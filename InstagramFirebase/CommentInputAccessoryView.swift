//
//  CommentInputAccessoryView.swift
//  InstagramFirebase
//
//  Created by Mahmoud on 9/8/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

protocol CommentInputAccessoryViewDelegate{
    func didSubmit(for comment: String)
}
class CommentInputAccessoryView: UIView {
    var delegate: CommentInputAccessoryViewDelegate?
    
    // submitButton
    private let submitButton: UIButton = {
        let sb = UIButton(type: .system)
        sb.setTitle("Submit", for: .normal)
        sb.setTitleColor(.black, for: .normal)
        sb.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        sb.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        return sb
    }()
    private let commentTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter comment"
        textField.resignFirstResponder()
        return textField
    }()
    
    func clearCommentTextField(){
        commentTextField.text = nil
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(submitButton)
        addSubview(commentTextField)
        
        submitButton.anchor(top: topAnchor, left: self.commentTextField.rightAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0.5, paddingLeft: 0, paddingBottom: 0, paddingRight: 12, width: 50, height: 0, centerX: nil, centerY: nil)
        
        commentTextField.anchor(top: topAnchor, left: leftAnchor, bottom:  bottomAnchor, right: submitButton.leftAnchor, paddingTop: 0.5, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, centerX: nil, centerY: nil)
        borderView()
    }
    
    private func borderView(){
        // borderView
        let borderView = UIView()
        borderView.backgroundColor = UIColor.rgb(red: 230, green: 230, blue: 230, alpha: 1)
        addSubview(borderView)
        borderView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: leftAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 1, centerX: nil, centerY: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func handleSubmit(){
        guard let commentText = commentTextField.text, !commentText.isEmpty else{ return }
        delegate?.didSubmit(for: commentText)
    }
    
}








//
//  PhotoSelectorHeader.swift
//  InstagramFirebase
//
//  Created by Mahmoud on 9/2/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit

class PhotoSelectorHeader: UICollectionReusableView {
    var selectedImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    private func setupViews(){
        addSubview(selectedImageView)
        selectedImageView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, centerX: nil, centerY: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

//
//  HomeController.swift
//  InstagramFirebase
//
//  Created by Mahmoud on 9/3/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

fileprivate let reuseIdentifier = "HomeCell"


class HomeController: UICollectionViewController, HomePostCellDelegate {
    
    var posts = [Post]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(SwipeRightToCamera))
        // swipeleft
        view.addGestureRecognizer(swipeRightGesture)
        swipeRightGesture.direction = .right
        
        // collectionView Setup
        collectionView?.backgroundColor = .white
        self.collectionView!.register(HomePostCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        // refreshControl
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
        
        // listen to posts update
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: SharePhotoController.updateFeedNotificationName, object: nil)
        
        setupNavigationItems()
        
        fetchAllPosts()
    }
    @objc func SwipeRightToCamera(gestureRecognizer: UISwipeGestureRecognizer){
        if gestureRecognizer.direction == .right{
            print("right swiped")
            handleCamera()
        }
    }
    @objc func handleUpdateFeed(){
        handleRefresh()
    }
    
    @objc func handleRefresh(){
        if posts.count == 0{
            
        }
        posts.removeAll()
        fetchAllPosts()
    }
    
    func fetchAllPosts(){
        fetchFollowingUserId()
        fetchPosts()
    }
    
    func setupNavigationItems(){
        navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "logo2"))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "camera3").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleCamera))
    }
    
   @objc func handleCamera(){
        let cameraController = CameraController()
        present(cameraController, animated: true, completion: nil)
    }
    
    private func fetchPosts(){
        guard let uid = Auth.auth().currentUser?.uid else { return }
        // extension
        Database.fetchUserWithUID(uid: uid) { (user) in
            self.fetchPostWithUser(user: user)
        }
    }
    fileprivate func fetchFollowingUserId(){
        guard let uid = Auth.auth().currentUser?.uid else{ return }
        Database.database().reference().child("following").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            guard let userIdsDictionaries = snapshot.value as? [String: Any] else{ return }
            userIdsDictionaries.forEach({ (key, value) in
                Database.fetchUserWithUID(uid: key, completion: { (user) in
                    self.fetchPostWithUser(user: user)
                })
            })
            
        }) { (error) in
            print("Failed to fetch following users", error)
        }
        
    }
    fileprivate func fetchPostWithUser(user: User){
        let postSRef = Database.database().reference().child("posts").child(user.uid)
        
        postSRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            self.collectionView?.refreshControl?.endRefreshing()
            if let dictionaries = snapshot.value as? [String: Any]{
                dictionaries.forEach({ (key, value) in
                    guard let dictionary = value as? [String: Any] else { return }
                    
                    var post = Post(user: user, dictionary: dictionary)
                    post.id = key
                    
                    guard let uid = Auth.auth().currentUser?.uid else{ return }
                    Database.database().reference().child("likes").child(key).child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
//                        print(snapshot.value)
                        if let value = snapshot.value as? Int, value == 1{
                            post.hasLiked = true
                        }else{
                            post.hasLiked = false
                        }
                        
                        self.posts.append(post)
                        self.posts.sort(by: { (p1, p2) -> Bool in
                            return p1.creationDate.compare(p2.creationDate) == .orderedDescending
                        })
                        self.collectionView?.reloadData()
                        
                    }, withCancel: { (error) in
                        print("failed to fetch likes")
                    })
                })
            }
            
        }) { (error) in
            print("Failed to fetch posts:", error)
        }
        
    }
    
    func didTabComment(post: Post) {
        let commentController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentController.post = post
        navigationController?.pushViewController(commentController, animated: true)
    }
    
    func didTabLike(for cell: HomePostCell) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        var post = posts[indexPath.item]
        guard let postId = post.id else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let values: [String: Any] = [uid: post.hasLiked == true ? 0 : 1]
        Database.database().reference().child("likes").child(postId).updateChildValues(values) { (error, ref) in
            if let error = error{
                print("Failed to like post", error)
            }
            
            post.hasLiked = !post.hasLiked
            self.posts[indexPath.item] = post
            
            self.collectionView?.reloadItems(at: [indexPath])
            print("successfully liked")
        }
    }
}

// MAKR: - DataSource
extension HomeController{
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomePostCell
        
        cell.postDelegate = self
        // fix index out of range crash
        if indexPath.item < posts.count{
            cell.post = self.posts[indexPath.item]
        }
        
        return cell
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
}

// MAKR: - UICollectionViewDelegateFlowLayout
extension HomeController: UICollectionViewDelegateFlowLayout{
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView?.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = view.frame.width
        var height: CGFloat = 40 + 8 + 8
        height += width
        height += 42
        height += 80
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}






















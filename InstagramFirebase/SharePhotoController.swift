//
//  SharePhotoController.swift
//  InstagramFirebase
//
//  Created by Mahmoud on 9/2/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class SharePhotoController: UIViewController, UITextViewDelegate {
    
    var selectedImage: UIImage?{
        didSet{
            imageView.image = selectedImage
        }
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    let textView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.becomeFirstResponder()
        return tv
    }()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    static let updateFeedNotificationName = Notification.Name(rawValue: "updateFeed")

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.rgb(red: 240, green: 240, blue: 240, alpha: 1)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: self, action: #selector(handleShare))
        
        textView.delegate = self
        setupImageAndTextViews()
    }
    
    fileprivate func setupImageAndTextViews(){
        let containerView = UIView()
        view.addSubview(containerView)
        containerView.backgroundColor = .white
        
        containerView.anchor(top: topLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 100, centerX: nil, centerY: nil)
        
        containerView.addSubview(imageView)
        imageView.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: nil, paddingTop: 8, paddingLeft: 8, paddingBottom: 8, paddingRight: 0, width: 84, height: 0, centerX: nil, centerY: nil)
        
        containerView.addSubview(textView)
        textView.anchor(top: containerView.topAnchor, left: imageView.rightAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, centerX: nil, centerY: nil)
    }
    
    @objc func handleShare(){
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        let fileName = NSUUID().uuidString
        guard let image = selectedImage else { return }
        guard let uploadData = UIImageJPEGRepresentation(image, 0.3) else{ return }
        let storageRef = Storage.storage().reference().child("posts").child(fileName)
        
        storageRef.putData(uploadData, metadata: nil) { (metadata, error) in
            if let err = error{
                print("Failed to upload image: ", err)
                return
            }
            
            storageRef.downloadURL(completion: { (url, error) in
                if let err = error{
                    print("failed to get profile image URL", err)
                    return
                }
                guard let imageUrl = url?.absoluteString else{ return }
                
                print("Successfully uploaded post image: ", imageUrl)
                self.savePostToDatabase(imageUrl: imageUrl)
                
            })
        }
    }
    
    fileprivate func savePostToDatabase(imageUrl: String){
        guard let postImage = selectedImage else { return }
        guard let caption = textView.text else{ return }
        guard let uid = Auth.auth().currentUser?.uid else{ return }
        
        let userPostRef = Database.database().reference().child("posts").child(uid)
        let ref = userPostRef.childByAutoId()
        let values: [String: Any] = [
            "caption": caption,
            "imageUrl": imageUrl,
            "imageWidth": postImage.size.width,
            "imageHeight": postImage.size.height,
            "creationDate": Date().timeIntervalSince1970
        ]
        
        ref.updateChildValues(values) { (error, ref) in
            if let err = error {
                print("Failed to save post to DB", err)
                return
            }
            NotificationCenter.default.post(name: SharePhotoController.updateFeedNotificationName, object: nil)
            
            print("successfully saved post to DB")
            self.dismiss(animated: true, completion: nil)
        }
    }
}








//
//  LoginViewController.swift
//  InstagramFirebase
//
//  Created by Mahmoud on 8/31/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    let logoContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 0, green: 120, blue: 175, alpha: 1)
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "Instagram_logo_white")
        imageView.contentMode = .scaleAspectFill
        
        view.addSubview(imageView)
        imageView.anchor(top: nil, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 200, height: 50, centerX: view.centerXAnchor, centerY: view.centerYAnchor)
        
        return view
        
    }()
    
    fileprivate func textField(with placeholder: String, isSecureText: Bool) -> UITextField {
        let tf = UITextField()
        tf.placeholder = placeholder
        tf.borderStyle = .roundedRect
        tf.isSecureTextEntry = isSecureText
        tf.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        return tf
    }
    lazy var emailTextField: UITextField = self.textField(with: "Email", isSecureText: false)
    lazy var passwordTextField: UITextField = self.textField(with: "Password", isSecureText: true)
    
    let logInButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Log In", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244, alpha: 1)
        button.isEnabled = false
        button.addTarget(self, action: #selector(handleLogIn), for: .touchUpInside)
        return button
    }()
    
    let dontHaveAccountButton: UIButton = {
        let button = UIButton(type: .system)
        let attributedTitle = NSMutableAttributedString(string: "Don't have an account?  ", attributes: [
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14),
            NSAttributedStringKey.foregroundColor: UIColor.lightGray
            ])
        
        attributedTitle.append(NSAttributedString(string: "Sign Up", attributes: [
            NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14),
            NSAttributedStringKey.foregroundColor: UIColor.rgb(red: 17, green: 154, blue: 237, alpha: 1)
            ]))
        
        button.setAttributedTitle(attributedTitle, for: .normal)
        
        button.addTarget(self, action: #selector(handleShowSignUp), for: .touchUpInside)
        return button
    }()
    @objc func handleLogIn(){
        guard let email = emailTextField.text, email.count > 0 else{ return }
        guard let password = passwordTextField.text, password.count > 0 else{ return }
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let error = error{
                print("Failed to login", error)
                return
            }
            print(result?.user.uid ?? "")
            
            self.presentMainTabBarController()
        }
    }
    
    public func presentMainTabBarController(){
        let mainTabBarController = MainTabBarController()
        self.present(mainTabBarController, animated: true, completion: nil)
    }
    
    @objc private func handleShowSignUp(){
        let signUpController = SignUpViewController()
        navigationController?.pushViewController(signUpController, animated: true)
    }
    
    @objc func textFieldDidChange(){
        let isValidForm = (emailTextField.text?.count)! > 0 &&
            (passwordTextField.text?.count)! > 0
        if isValidForm{
            logInButton.isEnabled = true
            logInButton.backgroundColor = UIColor.rgb(red: 17, green: 154, blue: 237, alpha: 1)
        }else{
            logInButton.isEnabled = false
            logInButton.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244, alpha: 1)
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationController?.isNavigationBarHidden = true
        view.addSubview(dontHaveAccountButton)
        view.addSubview(logoContainerView)
        setupSubViews()
        setupInputFields()
    }
    private func setupInputFields(){
        let inputFieldsStackView = UIStackView(arrangedSubviews: [emailTextField, passwordTextField, logInButton])
        inputFieldsStackView.axis = .vertical
        inputFieldsStackView.alignment = .fill
        inputFieldsStackView.distribution = .fillEqually
        inputFieldsStackView.spacing = 10
        view.addSubview(inputFieldsStackView)
        
        inputFieldsStackView.anchor(top: logoContainerView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 40, paddingLeft: 40, paddingBottom: 0, paddingRight: 40, width: 0, height: 140, centerX: nil, centerY: nil)
    }
    
    func setupSubViews(){
        logoContainerView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 150, centerX: nil, centerY: nil)
        
        dontHaveAccountButton.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50, centerX: nil, centerY: nil)
    }
    
    
}

//
//  SignUpViewController.swift
//  InstagramFirebase
//
//  Created by Mahmoud on 8/28/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase

class SignUpViewController: UIViewController{
    let loginViewController = LoginViewController()
    let addPhotoButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "plus_photo").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handleImagePicker), for: .touchUpInside)
        return button
    }()
    
    fileprivate func textField(with placeholder: String, isSecureText: Bool) -> UITextField {
        let tf = UITextField()
        tf.placeholder = placeholder
        tf.borderStyle = .roundedRect
        tf.isSecureTextEntry = isSecureText
        tf.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        return tf
    }
    lazy var emailTextField: UITextField = self.textField(with: "Email", isSecureText: false)
    lazy var passwordTextField: UITextField = self.textField(with: "Password", isSecureText: true)
    lazy var usernameTextField: UITextField =  self.textField(with: "Username", isSecureText: false)
    let signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign Up", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244, alpha: 1)
        button.isEnabled = false
        button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        return button
    }()
    let alreadyHaveAccountButton: UIButton = {
        let button = UIButton(type: .system)
        let attributedTitle = NSMutableAttributedString(string: "Already have an account?  ", attributes: [
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14),
            NSAttributedStringKey.foregroundColor: UIColor.lightGray
            ])
        
        attributedTitle.append(NSAttributedString(string: "Sign In", attributes: [
            NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14),
            NSAttributedStringKey.foregroundColor: UIColor.rgb(red: 17, green: 154, blue: 237, alpha: 1)
            ]))
        
        button.setAttributedTitle(attributedTitle, for: .normal)
        
        button.addTarget(self, action: #selector(handleShowLogin), for: .touchUpInside)
        return button
    }()
    
    @objc private func handleShowLogin(){
        navigationController?.popViewController(animated: true)
    }
    // MARK: - textField Validation
    @objc func textFieldDidChange(){
        let isValidForm = (emailTextField.text?.count)! > 0 &&
            (usernameTextField.text?.count)! > 0 &&
            (passwordTextField.text?.count)! > 0
        if isValidForm{
            signUpButton.isEnabled = true
            signUpButton.backgroundColor = UIColor.mainBlue()
        }else{
            signUpButton.isEnabled = false
            signUpButton.backgroundColor = UIColor.rgb(red: 149, green: 204, blue: 244, alpha: 1)
        }
    }
    
    // MARK: - signup logic
    @objc private func handleSignUp(){
        guard let email = emailTextField.text, email.count > 0 else{ return }
        guard let username = usernameTextField.text, username.count > 0 else{ return }
        guard let password = passwordTextField.text, password.count > 0 else{ return }
        
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let err = error{
                print("Failed to create user", err)
            }
            
            if result?.user != nil{
                guard let image = self.addPhotoButton.imageView?.image else{ return }
                
                guard let uploadData = UIImageJPEGRepresentation(image, 0.4) else{ return }
                let fileName = NSUUID().uuidString
                let storageRef = Storage.storage().reference().child("profile_images").child(fileName)
                
                storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                    if let err = error{
                        print("failed to upload profile image", err)
                        return
                    }
                    storageRef.downloadURL(completion: { (url, error) in
                        if let err = error{
                            print("failed to get profile image URL", err)
                            return
                        }
                        guard let profileImageUrl = url?.absoluteString else{ return }
                        guard let uid = result?.user.uid else{ return }
                        print("user created successfully", uid)
                        
                        let userValues = ["username": username, "profileImageUrl": profileImageUrl]
                        let values = [uid: userValues]
                        
                        Database.database().reference().child("users").updateChildValues(values, withCompletionBlock: { (error, reference) in
                            if let err = error{
                                print("Failed to add to database", err)
                            }
                            print("user added to database")
                            print(profileImageUrl)
                            
                            self.presentMainTabBarController()
                        })
                    })
                })
                
            }
        }
        
        
    }
    public func presentMainTabBarController(){
        let mainTabBarController = MainTabBarController()
        self.present(mainTabBarController, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(addPhotoButton)
        view.addSubview(alreadyHaveAccountButton)
        
        addPhotoButton.anchor(top: view.topAnchor, left: nil, bottom: nil, right: nil, paddingTop: 40, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 140, height: 140, centerX: view.centerXAnchor, centerY: nil)
        
        alreadyHaveAccountButton.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50, centerX: nil, centerY: nil)
        
        setupInputFields()
    }
    
    private func setupInputFields(){
        let inputFieldsStackView = UIStackView(arrangedSubviews: [emailTextField, usernameTextField, passwordTextField, signUpButton])
        inputFieldsStackView.axis = .vertical
        inputFieldsStackView.alignment = .fill
        inputFieldsStackView.distribution = .fillEqually
        inputFieldsStackView.spacing = 10
        view.addSubview(inputFieldsStackView)
        
        inputFieldsStackView.anchor(top: addPhotoButton.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 20, paddingLeft: 40, paddingBottom: 0, paddingRight: 40, width: 0, height: 200, centerX: nil, centerY: nil)
    }
    
}

// MARK: - handle imagePicker
extension SignUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func handleImagePicker(){
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            addPhotoButton.setImage(editedImage.withRenderingMode(.alwaysOriginal), for: .normal)
        }else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            addPhotoButton.setImage(originalImage.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        
        addPhotoButton.layer.cornerRadius = addPhotoButton.frame.width/2
        addPhotoButton.layer.masksToBounds = true
        addPhotoButton.layer.borderColor = UIColor.black.cgColor
        addPhotoButton.layer.borderWidth = 3
        
        dismiss(animated: true, completion: nil)
    }
}









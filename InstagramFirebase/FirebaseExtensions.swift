//
//  Extensions.swift
//  InstagramFirebase
//
//  Created by Mahmoud on 8/28/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import Firebase
import FirebaseDatabase

extension Database {
    static func fetchUserWithUID(uid: String, completion: @escaping (User) -> ()){
        let usersRef = Database.database().reference().child("users").child(uid)
        usersRef.observeSingleEvent(of: .value, with: { (snapshot) in
            guard let dictionary = snapshot.value as? [String: Any] else{ return }
            let user = User(uid: uid, dictionary: dictionary)
            completion(user)
            
        }) { (error) in
            print("Failed to fetch user in home controller: ", error)
        }
    }
}




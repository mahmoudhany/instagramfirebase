//
//  CommentCell.swift
//  InstagramFirebase
//
//  Created by Mahmoud on 9/6/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import SDWebImage
class CommentCell: UICollectionViewCell {
    
    var comment: Comment?{
        didSet{
            guard let imageUrl = self.comment?.user.profileImageUrl else { return }
            guard let url = URL(string: imageUrl) else { return }
            userProfileImageView.sd_setImage(with: url)
            setCommentLabelAttributedText()
        }
    }
    let commentTextView: UITextView = {
        let tv = UITextView()
        tv.isScrollEnabled = false
        tv.isUserInteractionEnabled = false
        return tv
    }()
    let userProfileImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 40/2
        return iv
    }()
    let borderView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return v
    }()
    func setCommentLabelAttributedText(){
        guard let comment = self.comment else { return }
        
        let attributedText = NSMutableAttributedString(string: comment.user.username, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
        
        attributedText.append(NSMutableAttributedString(string: " " + comment.text, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
        
        self.commentTextView.attributedText = attributedText
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(userProfileImageView)
        addSubview(commentTextView)
        addSubview(borderView)
        
        userProfileImageView.anchor(top: nil, left: leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, width: 40, height: 40, centerX: nil, centerY: centerYAnchor)
        
        commentTextView.anchor(top: topAnchor, left: userProfileImageView.rightAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 4, paddingLeft: 8, paddingBottom: 4, paddingRight: 4, width: 0, height: 0, centerX: nil, centerY: nil)
        
        borderView.anchor(top: nil, left: commentTextView.leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0.5, centerX: nil, centerY: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

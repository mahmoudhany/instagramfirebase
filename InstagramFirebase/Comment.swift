//
//  User.swift
//  InstagramFirebase
//
//  Created by Mahmoud on 9/3/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import Foundation

struct Comment{
    let user: User
    let uid: String
    let text: String
    let creationDate: Date
    
    init(user: User, dictionary: [String: Any]) {
        self.uid = dictionary["uid"] as? String ?? ""
        self.user = user
        text = dictionary["text"] as? String ?? ""
        
        let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
        self.creationDate = Date(timeIntervalSince1970: secondsFrom1970)
    }
}

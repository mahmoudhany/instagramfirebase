//
//  UserProfileViewController.swift
//  InstagramFirebase
//
//  Created by Mahmoud on 8/30/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

fileprivate let headerId = "UserProfileHeader"
fileprivate let photoCell = "PhotoCell"
fileprivate let homePostCell = "HomePostCell"

class UserProfileViewController: UICollectionViewController, UserProfileHeaderDelegate {
    
    
    var userId: String?
    var user: User?
    var posts = [Post]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        fetchUser()
        setupLogoutButton()
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateProfile), name: SharePhotoController.updateFeedNotificationName, object: nil)
    }
    @objc func handleUpdateProfile(){
        posts.removeAll()
        fetchUser()
    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        fetchUser()
//    }
    private func setupLogoutButton(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "gear").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleLogout))
        
    }
    @objc private func handleLogout(){
        print("Log out", "Cancel")
        let title = ("Log out", "Cancel")
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: title.0, style: .destructive, handler: { (_) in
            
            let loginViewController = LoginViewController()
            let navController = UINavigationController(rootViewController: loginViewController)
            
            self.present(navController, animated: true, completion: nil)
            
            do {
                try Auth.auth().signOut()
            } catch let signOutError {
                print("Failed to sign out", signOutError)
            }
            
            
        }))
        alertController.addAction(UIAlertAction(title: title.1, style: .cancel, handler: { (_) in
            print("cancel")
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    private func setupCollectionView(){
        collectionView?.backgroundColor = .white
        collectionView?.register(UserProfileHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
        
        collectionView?.register(UserProfilePhotoCell.self, forCellWithReuseIdentifier: photoCell)
        collectionView?.register(HomePostCell.self, forCellWithReuseIdentifier: homePostCell)
    }
    
    var isFinishedPaging = false
    
    func paginatePosts(){
        guard let uid = self.user?.uid else { return }
        let ref = Database.database().reference().child("posts").child(uid)
        var query = ref.queryOrdered(byChild: "creationDate")
        
        if posts.count > 0 {
            let value = posts.last?.creationDate.timeIntervalSince1970
            query = query.queryEnding(atValue: value)
        }
        
        query.queryLimited(toLast: 4).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard var allObjects = snapshot.children.allObjects as? [DataSnapshot] else { return }
            
            allObjects.reverse()
            
            if allObjects.count < 4 {
                self.isFinishedPaging = true
            }
            
            if self.posts.count > 0 && allObjects.count > 0 {
                allObjects.removeFirst()
            }
            
            guard let user = self.user else { return }
            
            allObjects.forEach({ (snapshot) in
                
                guard let dictionary = snapshot.value as? [String: Any] else { return }
                var post = Post(user: user, dictionary: dictionary)
                post.id = snapshot.key
                
                self.posts.append(post)
            })
            
            self.posts.forEach({ (post) in
                print(post.id ?? "")
            })
            
            self.collectionView?.reloadData()
        }) { (err) in
            print("Failed to paginate for posts:", err)
        }
        
    }
    
    private func fetchUser(){
        let uid = userId ?? (Auth.auth().currentUser?.uid ?? "")
        Database.fetchUserWithUID(uid: uid) { (user) in
            self.user = user
            self.navigationItem.title = self.user?.username
            self.collectionView?.reloadData()
            self.paginatePosts()
        }
        
    }
    
    private func fetchOrderedPosts(){
        guard let uid = user?.uid else { return }
        let ref = Database.database().reference().child("posts").child(uid)

        // MARK: Add pagination here
        ref.queryOrdered(byChild: "creationDate").observe(.childAdded, with: { (snapshot) in
            guard let dictionary = snapshot.value as? [String: Any] else { return }
            guard let user = self.user else{ return }

            let post = Post(user: user, dictionary: dictionary)

            self.posts.insert(post, at: 0)
            self.collectionView?.reloadData()

        }) { (error) in
            print("Failed to fetch posts:", error)
        }
    }
    
    /******************* Layout Changing *******************/
    var isGridView = true
    func didChangeToGridView() {
        print("didChangeToGridView")
        isGridView = true
        collectionView?.reloadData()
    }
    func didChangeToListView() {
        isGridView = false
        collectionView?.reloadData()
    }
    
}

// MAKR: - DataSource
extension UserProfileViewController{
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == posts.count-1 && !isFinishedPaging{
            paginatePosts()
        }
        
        if isGridView == true{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: photoCell, for: indexPath) as! UserProfilePhotoCell
            cell.post = posts[indexPath.item]
            
            return cell
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homePostCell, for: indexPath) as! HomePostCell
            cell.post = posts[indexPath.item]
            return cell
        }
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! UserProfileHeader
        
        header.user = user
        header.delegate = self
        
        return header
    }
    
}

// MARK: - FlowLayoutDelegate
extension UserProfileViewController: UICollectionViewDelegateFlowLayout{
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView?.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isGridView == true{
            let width = (view.frame.width - 2) / 3
            return CGSize(width: width, height: width)
        }else{
            let width = view.frame.width
            var height: CGFloat = 40 + 8 + 8
            height += width
            height += 42
            height += 80
            return CGSize(width: width, height: height)
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 200)
    }
    
}

















//
//  User.swift
//  InstagramFirebase
//
//  Created by Mahmoud on 9/3/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import Foundation

struct User{
    let uid: String
    let username: String
    let profileImageUrl: String
    
    init(uid: String, dictionary: [String: Any]) {
        self.uid = uid
        username = dictionary["username"] as? String ?? ""
        profileImageUrl = dictionary["profileImageUrl"] as? String ?? ""
    }
}

//
//  PhotoSelectorCell.swift
//  InstagramFirebase
//
//  Created by Mahmoud on 9/1/18.
//  Copyright © 2018 Mahmoud. All rights reserved.
//

import UIKit
import SDWebImage

class UserProfilePhotoCell: UICollectionViewCell {
    var post: Post?{
        didSet{
            DispatchQueue.main.async {
                guard let imageUrl = self.post?.imageUrl else { return }
                guard let url = URL(string: imageUrl) else { return }
                self.photoImageView.sd_setImage(with: url)
            }
        }
    }
    
    let photoImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(photoImageView)
        photoImageView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, centerX: nil, centerY: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
